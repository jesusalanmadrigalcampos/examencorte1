package com.example.examentipob;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina reciboNomina;
    private TextView lblNumRecibo;
    private TextView lblNombre;
    private EditText txtHorasNormales;
    private EditText txtHorasExtras;
    private RadioButton rdbAuxiliar;
    private RadioButton rdbAlbañil;
    private RadioButton rdbIng;

    private TextView lblImpuesto;
    private TextView lblSubtotal;
    private TextView lblTotalPagar;

    private Button btnRegresar;
    private Button btnLimpiar;
    private Button btnCalcular;

    private RadioGroup opPuesto;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        lblNumRecibo = (TextView) findViewById(R.id.lblNumRecibo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtHorasNormales = (EditText) findViewById(R.id.txtHorasNormales);
        txtHorasExtras = (EditText) findViewById(R.id.lblHorasExtras);

        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        txtValor = (EditText) findViewById(R.id.txtValor);
        txtPorcentaje = (EditText) findViewById(R.id.txtPorcentaje);

        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarVacios()) {
                    hacerNomina();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText("");
                txtValor.setText("");
                txtPorcentaje.setText("");
                lblPago.setText("");
                lblEnganche.setText("");
            }
        });

        public boolean validarVacios () {
            boolean b = true;

            if (txtHorasNormales.getText().toString().trim().matches("")) {
                Toast.makeText(ReciboNominaActivity.this, "Ingrese una descripcion del auto", Toast.LENGTH_SHORT).show();
                b = false;
            } else if (txtHorasExtras.getText().toString().trim().matches("")) {
                Toast.makeText(ReciboNominaActivity.this, "Ingrese el valor del auto", Toast.LENGTH_SHORT).show();
                b = false;
            } else if (txtPorcentaje.getText().toString().trim().matches("")) {
                Toast.makeText(ReciboNominaActivity.this, "Ingrese el porcentaje de enganche", Toast.LENGTH_SHORT).show();
                b = false;
            }
            return b;
        }

        public int seleccionarPuesto () {
            int h = 0;
            int e = 0;
            if (opPuesto.getCheckedRadioButtonId() == R.id.rdbAuxiliar) {
                h = 50;
                e = 100;
            } else if (opPuesto.getCheckedRadioButtonId() == R.id.rdbAlbañil) {
                h = 70;
                e = 140;
            } else if (opPuesto.getCheckedRadioButtonId() == R.id.rdbIng) {
                h = 100;
                e = 200;

            }
            return h;
        }
    }
}
