package com.example.examentipob;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {

    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto)
    {
        this.numRecibo = this.generaRecibo();
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
    }

    public int generaRecibo(){
        return new Random().nextInt(1000+1)+1;
    }

    public ReciboNomina() {
        this.numRecibo= this.generaRecibo();
    }

    // Set y Get

    public int getNumRecibo() {
        return numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float calcularSubtotal()
    {
        float subtotal = horasTrabNormal*horasTrabNormal+horasTrabExtras*horasTrabExtras;
        return subtotal;
    }

    public float calcularImpuesto()
    {
        float impuesto = 0.16f;
        impuesto = calcularSubtotal() * impuesto;
        return impuesto;
    }

    public float calcularTotal()
    {
        float total = calcularSubtotal() - calcularImpuesto();
        return  total;
    }
}
