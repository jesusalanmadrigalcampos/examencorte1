package com.example.examentipob;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.examentipob.R.string.*;
import static com.example.examentipob.R.string.lblNombre;

public class MainActivity extends AppCompatActivity {

    private ReciboNomina reciboNomina;
    private EditText txtNombre;
    private Button btnEntrar;
    private  Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre=(EditText) findViewById(R.id.txtNombre);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnSalir=(Button)findViewById(R.id.btnSalir);
        reciboNomina = new ReciboNomina();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar nombre",Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                    //Enviar dato String
                    intent.putExtra("cliente",nombre);

                    //enviar un objeto
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("cotizacion",reciboNomina);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
